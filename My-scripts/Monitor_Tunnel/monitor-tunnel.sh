#!/bin/sh

str="forwarding requests processed"

while true
do
    #Check if the tunnel is working with autossh
    oldpid=$(pidof autossh)
    echo "$(date) OLD PID is ${oldpid}" >> /var/log/sshtunnel.log 2>&1
    sh auto-sshreach.sh >autossh.log 2>&1 &
    sleep 10
    if grep -q "$str" autossh.log
    then
        echo "$(date) Tunnel is active" >> /var/log/sshtunnel.log 2>&1
        sleep 5m
        if [ -n "${oldpid}" ]; then
            kill -9 $oldpid
        fi
    else
		echo "$(date) Tunnel is NOT active" >> /var/log/sshtunnel.log 2>&1
        if [ -n "${oldpid}" ]; then
            kill -9 ${oldpid}
        fi
        sleep 5
        newpid=$(pidof autossh)
        echo "$(date) NEW PID is ${newpid}" >> /var/log/sshtunnel.log 2>&1
        if [ -n "${newpid}" ]; then
            kill -9 ${newpid}
        fi
		systemctl restart ppp-rk.service
        sleep 10
        continue
    fi
    sleep 60
done


